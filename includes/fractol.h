/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/05 17:34:00 by clrichar          #+#    #+#             */
/*   Updated: 2019/09/04 18:58:12 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include <stdlib.h>
# include <unistd.h>
# include <math.h>
# include "mlx.h"
# include "libft.h"

# define WIN1_W 800
# define WIN1_H 600

# define WIN data->g_env
# define IMG data->img

# ifdef __linux__
#  define HOOKLEFT 65361 
#  define HOOKTOP 65362
#  define HOOKRIGHT 65363
#  define HOOKDOWN 65364
#  define HOOKONE 49
#  define HOOKTWO 50
#  define HOOKDASH 45
#  define HOOKEQUAL 61
#  define HOOKC 99
#  define HOOKZERO 48
#  define HOOKESCAPE 65307
# elif __APPLE__
#  define HOOKLEFT 0x7B
#  define HOOKTOP 0x7E
#  define HOOKRIGHT 0x7C
#  define HOOKDOWN 0x7D
#  define HOOKONE 0x12
#  define HOOKTWO 0x13
#  define HOOKDASH 0x1B
#  define HOOKEQUAL 0x18
#  define HOOKC 0x08
#  define HOOKZERO 0x1D
#  define HOOKESCAPE 0x35
# endif

typedef struct		s_env
{
	void			*mlx_ptr;
	void			*win_ptr;
	void			*img_ptr;
	int				win_w;
	int				win_h;
	double			off_x;
	double			off_y;
}					t_env;

typedef struct		s_img
{
	char			*addr;
	int				bpp;
	int				sizeline;
	int				endian;
}					t_img;

typedef struct		s_data
{
	t_env			g_env;
	t_img			img;
	int				init;
	int				julia_act;
	int				imax;
	int				fid;
	int				color;
	int				hue;
	int				mouse_x;
	int				mouse_y;
	double			x1;
	double			x2;
	double			y1;
	double			y2;
	double			cr;
	double			ci;
	double			zr;
	double			zi;
	double			zoom;
}					t_data;

void				p_mandelbrot(int x, int y, t_data *data);
void				p_julia(int x, int y, t_data *data);
void				p_trihorn(int x, int y, t_data *data);
void				m_print(int index, t_data *data);
int					m_hook(int key, t_data *data);
int					m_rhook(int key, t_data *data);
int					m_mouse(int key, int x, int y, t_data *data);
int					m_mouse_pos(int x, int y, t_data *data);
void				f_fractol(int i, t_data *data);
void				f_zoom(int act, int x, int y, t_data *data);
void				f_reset(t_data *data);
void				f_julia_act(t_data *data);
void				mlx_pixel_to_img(t_data *data, int x, int y, int color);
void				mlx_redraw_img(t_data *data);

#endif
