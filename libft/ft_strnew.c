/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:46 by clrichar          #+#    #+#             */
/*   Updated: 2018/01/27 17:33:46 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strnew(size_t size)
{
	char		*s;

	if (!(s = (char *)malloc(sizeof(char) * (size + 1))))
		return (NULL);
	while (size > 0)
		*(s + size--) = '\0';
	*(s + size) = '\0';
	return (s);
}
