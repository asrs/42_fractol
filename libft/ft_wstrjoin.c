/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wstrjoin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:50 by clrichar          #+#    #+#             */
/*   Updated: 2018/01/27 17:33:51 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

wchar_t			*ft_wstrjoin(const wchar_t *s1, const wchar_t *s2)
{
	wchar_t		*dest;

	if (!(s1 && s2))
		return (NULL);
	else if (!(dest = ft_wstrnew(ft_wstrlen(s1) + ft_wstrlen(s2))))
		return (NULL);
	if (s1)
		dest = ft_wstrcat(dest, s1);
	if (s2)
		dest = ft_wstrcat(dest, s2);
	return (dest);
}
