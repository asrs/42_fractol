/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/03 15:22:38 by clrichar          #+#    #+#             */
/*   Updated: 2018/03/05 17:32:39 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

#define WIN_W WIN1_W
#define WIN_H WIN1_H

void			f_julia_act(t_data *data)
{
	if (data->julia_act == 0)
		data->julia_act = 1;
	else if (data->julia_act == 1)
		data->julia_act = 0;
}

void			f_reset(t_data *data)
{
	data->hue = 0;
	data->init = 0;
	data->zoom = (WIN_W / 4) + 10;
}

void			f_fractol(int i, t_data *data)
{
	if (i == 0 && data->fid < 2)
		data->fid++;
	else if (i == 0 && data->fid >= 2)
		data->fid = 0;
	if (i == 1 && data->fid > 0)
		data->fid--;
	else if (i == 1 && data->fid <= 0)
		data->fid = 2;
	f_reset(data);
}

void			mlx_pixel_to_img(t_data *data, int x, int y, int color)
{
	int			color1;
	int			color2;
	int			color3;
	int			bit_pix;
	float		img_size;

	img_size = (float)(WIN1_W * WIN1_H * IMG.bpp / 8);
	if (x < 0 || y < 0 || y * IMG.sizeline + x * \
			IMG.bpp / 8 > img_size || x >= IMG.sizeline /\
			(IMG.bpp / 8) || y >= (int)img_size / IMG.sizeline)
		return ;
	color1 = color;
	color2 = color >> 8;
	color3 = color >> 16;
	bit_pix = IMG.bpp / 8;
	IMG.addr[y * IMG.sizeline + x * bit_pix] = (char)color1;
	IMG.addr[y * IMG.sizeline + x * bit_pix + 1] = (char)color2;
	IMG.addr[y * IMG.sizeline + x * bit_pix + 2] = (char)color3;
}

void			mlx_redraw_img(t_data *data)
{
	mlx_destroy_image(WIN.mlx_ptr, WIN.img_ptr);
	WIN.img_ptr = NULL;
	m_print(data->fid, data);
}
