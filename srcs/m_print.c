/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_print.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/03 23:05:18 by clrichar          #+#    #+#             */
/*   Updated: 2018/03/05 17:32:40 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

#define WIN_W WIN1_W
#define WIN_H WIN1_H

void			m_print(int index, t_data *data)
{
	WIN.img_ptr = mlx_new_image(WIN.mlx_ptr, (int)WIN_W, (int)WIN_H);
	IMG.addr = mlx_get_data_addr(WIN.img_ptr, &IMG.bpp, \
			&IMG.sizeline, &IMG.endian);
	if (index == 0)
		p_mandelbrot(0, 0, data);
	if (index == 1)
		p_julia(0, 0, data);
	if (index == 2)
		p_trihorn(0, 0, data);
	mlx_put_image_to_window(WIN.mlx_ptr, WIN.win_ptr, WIN.img_ptr, 0, 0);
}
