/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_hook.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/03 23:18:31 by clrichar          #+#    #+#             */
/*   Updated: 2019/09/04 18:59:16 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

#define WIN_W WIN1_W
#define WIN_H WIN1_H

void			f_zoom(int act, int x, int y, t_data *data)
{
	double		tmpx;
	double		tmpy;
	double		tmpx2;
	double		tmpy2;

	tmpx = data->x1 + x * (data->x2 - data->x1) / WIN_W;
	tmpy = data->y1 + y * (data->y2 - data->y1) / WIN_H;
	tmpx2 = data->x1;
	tmpy2 = data->y1;
	if (act)
	{
		data->zoom *= 2;
		data->x1 = tmpx - (data->x2 - data->x1) / 4;
		data->x2 = tmpx + (data->x2 - tmpx2) / 4;
		data->y1 = tmpy - (data->y2 - data->y1) / 4;
		data->y2 = tmpy + (data->y2 - tmpy2) / 4;
	}
	else
	{
		data->zoom /= 2;
		data->x1 = tmpx - (data->x2 - data->x1);
		data->x2 = tmpx + (data->x2 - tmpx2);
		data->y1 = tmpy - (data->y2 - data->y1);
		data->y2 = tmpy + (data->y2 - tmpy2);
	}
}

static void		move(int i, t_data *data)
{
	int			x;
	int			y;

	x = WIN_W / 2;
	y = WIN_H / 2;
	(i == 0) ? x -= 25 : 0;
	(i == 1) ? x += 25 : 0;
	(i == 2) ? y -= 25 : 0;
	(i == 3) ? y += 25 : 0;
	f_zoom(1, x, y, data);
	f_zoom(0, x, y, data);
}

static void		change_hue(t_data *data)
{
	if (data->hue < 0xffffff)
		data->hue += 0x101010;
	else if (data->hue >= 0xffffff)
		data->hue = 0;
}

static void		change_imax(int i, t_data *data)
{
	if (i == 0 && data->imax < 550)
		data->imax += 1;
	if (i == 1 && data->imax > 3)
		data->imax -= 1;
}

int				m_hook(int key, t_data *data)
{
	(key == HOOKLEFT) ? move(1, data) : 0;
	(key == HOOKRIGHT) ? move(0, data) : 0;
	(key == HOOKDOWN) ? move(2, data) : 0;
	(key == HOOKTOP) ? move(3, data) : 0;
	(key == HOOKEQUAL) ? f_zoom(1, (WIN_W / 2), (WIN_H / 2), data) : 0;
	(key == HOOKDASH) ? f_zoom(0, (WIN_W / 2), (WIN_H / 2), data) : 0;
	(key == HOOKONE) ? change_imax(0, data) : 0;
	(key == HOOKTWO) ? change_imax(1, data) : 0;
	(key == HOOKC) ? change_hue(data) : 0;
	(key == HOOKZERO) ? f_reset(data) : 0;
	(key == HOOKESCAPE) ? exit(EXIT_SUCCESS) : 0;
	mlx_redraw_img(data);
	return (0);
}
